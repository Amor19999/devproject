# Встановлення контейнерів за допомогою Docker-Compose.  

### Для початку потрібно створити файл **docker-compose.yml, compose.yaml** .... .  

### Файл **docker-compose.yml** є default file for **docker compose**.  

###  Пам'ятай що має бути встановлений docker та docker compose.  

### Для початку зробіть клон: ``git clone https://gitlab.com/Amor19999/devproject.git``. 

### Перейдіть в теку ``cd devproject`` .  

### Виконайте команду ``docker-compose up -d``  

### І все, ваш застосунок запущено на 80 та 8080 порту!  

### Для зупинки контейнерів введіть команду ``docker-compose down``  

### Якщо недостатньо місця на диску і використовували купу docker images  

### Зробіть наступне:  

        docker-compose down   - зупинить контейнери які запускалися за допомогою Docker-Compose  

        docker container ls -a   - Перевірка наявності запущених контейнерів  

        docker container stop #id_container1# #id_container2# #id_container3# якщо є виконуємо та підставляємо id  
        наших контейнерів, якщо немає пропускаємо   

        docker system prune   - Видалить всі images які колись завантажувались і всі пов’язані файли з ними!  


### Пам'ятайте! Порти мають бути відкриті та незайняті іншими застосунками!  

### Якщо ви використовуєте *Cloud instance* треба перевірити *Security Grope* на відкриті порти теж!  

