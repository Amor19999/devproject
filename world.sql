CREATE TABLE task ( 
    id SERIAL PRIMARY KEY NOT NULL, 
    task text UNIQUE, 
    status INTEGER DEFAULT 0 
);

ALTER TABLE task OWNER TO todo_user;